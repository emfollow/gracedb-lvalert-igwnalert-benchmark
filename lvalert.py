#!/usr/bin/env python
from sleek_lvalert import LVAlertClient
import json
from time import time
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-p', '--playground', help='Use playground', action='store_true')
parser.add_argument('graceid', type=str)
args = parser.parse_args()

if args.playground:
    client = LVAlertClient(server='lvalert-playground.cgca.uwm.edu')
    filename = 'lvalert-playground.txt'
else:
    client = LVAlertClient()
    filename = 'lvalert.txt'

f = open(filename, 'w')
print('uuid', 'time', file=f)

def received(_, payload):
    t = time()
    params = json.loads(payload)

    if params['alert_type'] == 'log' and params['uid'] == args.graceid:
        uuid = params['data']['comment']
        print(uuid, t, file=f)
        f.flush()

client.connect()
client.process()
client.listen(received)
