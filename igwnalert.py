#!/usr/bin/env python
from igwn_alert import client
from time import time
import argparse
import json

parser = argparse.ArgumentParser()
parser.add_argument('-p', '--playground', help='Use playground', action='store_true')
parser.add_argument('graceid', type=str)
args = parser.parse_args()

if args.playground:
    c = client(group='gracedb-playground')
    filename = 'igwnalert-playground.txt'
else:
    c = client()
    filename = 'igwnalert.txt'

f = open(filename, 'w')
print('uuid', 'time', file=f)

def received(topic, payload):
    t = time()
    payload = json.loads(payload)

    if payload['alert_type'] == 'log' and payload['uid'] == args.graceid:
        uuid = payload['data']['comment']
        print(uuid, t, file=f)
        f.flush()

c.listen(received, 'mdc_superevent')
