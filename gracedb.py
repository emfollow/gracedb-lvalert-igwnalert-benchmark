#!/usr/bin/env python
from gracedb_sdk import Client
from uuid import uuid1
from time import time, sleep
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-p', '--playground', help='Use playground', action='store_true')
parser.add_argument('graceid', type=str)
args = parser.parse_args()

if args.playground:
    client = Client('https://gracedb-playground.ligo.org/api/')
    filename = 'gracedb-playground.txt'
else:
    client = Client()
    filename = 'gracedb.txt'

with open(filename, 'w') as f:
    print('uuid', 'time', file=f)
    while True:
        uuid = str(uuid1())
        t = time()
        client.superevents[args.graceid].logs.create(comment=uuid)
        print(uuid, t, file=f)
        f.flush()
        sleep(2)
